package com.sciensa.repository;

import com.sciensa.models.entities.Artist;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {
    List<Artist> findAllByFirstNameContainsOrLastNameContains(String firstName, String lastName, Pageable page);
}
