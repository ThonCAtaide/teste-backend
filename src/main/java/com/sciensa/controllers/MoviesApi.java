package com.sciensa.controllers;

import com.sciensa.models.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/movies")
public interface MoviesApi {

    @ApiOperation(value = "Cadastra um novo filme", nickname = "addMovie", notes = "", response = Object.class, tags={ "movies", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Filme criado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity addMovie(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Movie movie);


    @ApiOperation(value = "Detalhe de filme", nickname = "getMovie", notes = "", response = Object.class, tags={ "movies", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Filme consultado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a consulta - server side", response = ResponseError.class) })
    @GetMapping(value = "/{movieId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getMovie(@ApiParam(value = "",required=true) @PathVariable("movieId") Long movieId);

    @ApiOperation(value = "Lista os filmes cinematográficos", nickname = "listMovies", notes = "", response = Object.class, tags={ "movies", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Filmes consultados com sucesso", response = ResponseListObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity listMovies(@ApiParam(value = "Página da listagem a ser retornada", defaultValue = "1") @Valid @RequestParam(value = "page", required = false, defaultValue="1") Integer page, @ApiParam(value = "Tamanho da paginação a ser utilizada no request", defaultValue = "10") @Valid @RequestParam(value = "size", required = false, defaultValue="10") Integer size, @ApiParam(value = "Retorna itens cuja descrição se pareça com o valor informado") @Valid @RequestParam(value = "search", required = false) String search);


    @ApiOperation(value = "Remove um filme do catálogo", nickname = "removeMovie", notes = "", response = Object.class, tags={ "movies", })
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Filme removido com sucesso"),
            @ApiResponse(code = 404, message = "Filme não encontrado", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a deleção - server side", response = ResponseError.class) })
    @DeleteMapping(value = "/{movieId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity removeMovie(@ApiParam(value = "",required=true) @PathVariable("movieId") Long movieId);


    @ApiOperation(value = "Atualização de filme", nickname = "updateMovie", notes = "", response = Object.class, tags={ "movies", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Filme atualizado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @PutMapping(value = "/{movieId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity updateMovie(@ApiParam(value = "",required=true) @PathVariable("movieId") Long movieId,@ApiParam(value = "" ,required=true )  @Valid @RequestBody Movie movie);
}
