package com.sciensa.controllers;

import com.sciensa.models.dto.Movie;
import com.sciensa.service.ApiService;
import com.sciensa.service.MovieService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(tags = {"movies"})
public class MoviesApiController implements MoviesApi {

    private ApiService service;

    @Autowired
    public MoviesApiController(MovieService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity addMovie(@Valid Movie movie) {
        return service.create(movie);
    }

    @Override
    public ResponseEntity getMovie(Long movieId) {
        return service.getById(movieId);
    }

    @Override
    public ResponseEntity listMovies(@Valid Integer page, @Valid Integer size, @Valid String search) {
        return service.getAll(page, size, search);
    }

    @Override
    public ResponseEntity removeMovie(Long movieId) {
        return service.delete(movieId);
    }

    @Override
    public ResponseEntity updateMovie(Long movieId, @Valid Movie movie) {
        return service.update(movieId, movie);
    }

}
