package com.sciensa.controllers;

import com.sciensa.models.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/genres")
public interface GenresApi {

    @ApiOperation(value = "Cadastra um novo gênero cinematográfico", nickname = "addGenre", notes = "", response = Object.class, tags = {"genres",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gênero criado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class)})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity addGenre(@ApiParam(value = "", required = true) @Valid @RequestBody Genre genre);


    @ApiOperation(value = "Detalhe de gênero cinematográfico", nickname = "getGenre", notes = "", response = Object.class, tags = {"genres",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gênero consultado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class)})
    @GetMapping(value = "/{genreId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getGenre(@ApiParam(value = "", required = true) @PathVariable("genreId") Long genreId);


    @ApiOperation(value = "Lista os gêneros cinematográficos", nickname = "listGenres", notes = "", response = Object.class, tags = {"genres",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gêneros consultados com sucesso", response = ResponseListObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class)})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity listGenres(@ApiParam(value = "Página da listagem a ser retornada", defaultValue = "1") @Valid @RequestParam(value = "page", required = false, defaultValue = "1") Integer page, @ApiParam(value = "Tamanho da paginação a ser utilizada no request", defaultValue = "10") @Valid @RequestParam(value = "size", required = false, defaultValue = "10") Integer size, @ApiParam(value = "Retorna itens cuja descrição se pareça com o valor informado") @Valid @RequestParam(value = "search", required = false) String search);


    @ApiOperation(value = "Atualização de gênero cinematográfico", nickname = "updateGenre", notes = "", response = Object.class, tags = {"genres",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Gênero atualizado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class)})
    @PutMapping(value = "/{genreId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity updateGenre(@ApiParam(value = "", required = true) @PathVariable("genreId") Long genreId, @ApiParam(value = "", required = true) @Valid @RequestBody Genre genre);
}

