package com.sciensa.controllers;

import com.sciensa.models.dto.Artist;
import com.sciensa.service.ApiService;
import com.sciensa.service.ArtistService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(tags = {"artists"})
public class ArtistsApiController implements ArtistsApi {

    private ApiService service;

    @Autowired
    public ArtistsApiController(ArtistService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity addArtist(@Valid Artist artist) {
        return service.create(artist);
    }

    @Override
    public ResponseEntity getArtist(Long artistId) {
        return service.getById(artistId);
    }

    @Override
    public ResponseEntity getArtistFilmography(Long artistId) {
        return ((ArtistService) service).getArtistFilmography(artistId);
    }

    @Override
    public ResponseEntity listArtists(@Valid Integer page, @Valid Integer size, @Valid String search) {
        return service.getAll(page, size, search);
    }

    @Override
    public ResponseEntity updateArtist(Long artistId, @Valid Artist artist) {
        return service.update(artistId, artist);
    }

}
