package com.sciensa.controllers;

import com.sciensa.models.dto.Genre;
import com.sciensa.service.ApiService;
import com.sciensa.service.GenreService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(tags = {"genres"})
public class GenreApiController implements GenresApi {

    private ApiService service;

    @Autowired
    public GenreApiController(GenreService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity addGenre(@Valid Genre genre) {
        return service.create(genre);
    }

    @Override
    public ResponseEntity getGenre(Long genreId) {
        return service.getById(genreId);
    }

    @Override
    public ResponseEntity listGenres(@Valid Integer page, @Valid Integer size, @Valid String search) {
        return service.getAll(page, size, search);
    }

    @Override
    public ResponseEntity updateGenre(Long genreId, @Valid Genre genre) {
        return service.update(genreId, genre);
    }
}
