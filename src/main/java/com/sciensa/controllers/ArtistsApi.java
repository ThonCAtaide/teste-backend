package com.sciensa.controllers;

import com.sciensa.models.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/artist")
public interface ArtistsApi {

    @ApiOperation(value = "Cadastra um novo artista", nickname = "addArtist", notes = "", response = Object.class, tags={ "artists", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Artista criado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity addArtist(@ApiParam(value = "", required = true) @Valid @RequestBody Artist artist);


    @ApiOperation(value = "Detalhe de artista", nickname = "getArtist", notes = "", response = Object.class, tags={ "artists", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Artista consultado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @GetMapping(value = "/{artistId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getArtist(@ApiParam(value = "", required = true) @PathVariable("artistId") Long artistId);


    @ApiOperation(value = "Filmografia", nickname = "getArtistFilmography", notes = "", response = Object.class, tags={ "artists", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Filmografia consultada com sucesso", response = ResponseListObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a consulta - server side", response = ResponseError.class) })
    @GetMapping(value = "/{artistId}/filmography", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getArtistFilmography(@ApiParam(value = "", required = true) @PathVariable("artistId") Long artistId);


    @ApiOperation(value = "Lista os artistas", nickname = "listArtists", notes = "", response = Object.class, tags={ "artists", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Artistas consultados com sucesso", response = ResponseListObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity listArtists(@ApiParam(value = "Página da listagem a ser retornada", defaultValue = "1") @Valid @RequestParam(value = "page", required = false, defaultValue = "1") Integer page, @ApiParam(value = "Tamanho da paginação a ser utilizada no request", defaultValue = "10") @Valid @RequestParam(value = "size", required = false, defaultValue = "10") Integer size, @ApiParam(value = "Retorna itens cuja descrição se pareça com o valor informado") @Valid @RequestParam(value = "search", required = false) String search);


    @ApiOperation(value = "Atualização de artista", nickname = "updateArtist", notes = "", response = Object.class, tags={ "artists", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Artista atualizado com sucesso", response = ResponseObject.class),
            @ApiResponse(code = 400, message = "Parâmetros invalidos - client side", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro durante a criação - server side", response = ResponseError.class) })
    @PutMapping(value = "/{artistId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity updateArtist(@ApiParam(value = "", required = true) @PathVariable("artistId") Long artistId, @ApiParam(value = "", required = true) @Valid @RequestBody Artist artist);
}