package com.sciensa.models.dto;

import java.util.List;

public class ResponseListObject implements Response {

    private List<Record> records;
    private ResponseMeta meta;

    public ResponseListObject() {
    }

    public ResponseListObject(List<Record> records, ResponseMeta meta) {
        this.records = records;
        this.meta = meta;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public ResponseMeta getMeta() {
        return meta;
    }

    public void setMeta(ResponseMeta meta) {
        this.meta = meta;
    }
}
