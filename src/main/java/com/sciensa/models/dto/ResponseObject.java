package com.sciensa.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseObject implements Response {

    private Record record;

    private ResponseMeta meta;

    public ResponseObject() {
    }

    public ResponseObject(Record record, ResponseMeta meta) {
        this.record = record;
        this.meta = meta;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public ResponseMeta getMeta() {
        return meta;
    }

    public void setMeta(ResponseMeta meta) {
        this.meta = meta;
    }
}
