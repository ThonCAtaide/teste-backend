package com.sciensa.models.entities;

import com.sciensa.utils.DateAndTimeUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "Genre")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String description;

    @Column
    private Timestamp createdAt;

    @Column
    private Timestamp updatedAt;

    @ManyToMany(mappedBy = "genres")
    private List<Movie> movies;

    public Genre() {
    }

    public Genre(com.sciensa.models.dto.Genre genre) {
        this.id = genre.getId();
        this.description = genre.getDescription();
        this.createdAt = DateAndTimeUtils.convertOffsetInTimeStamp(genre.getCreatedAt());
        this.updatedAt = DateAndTimeUtils.convertOffsetInTimeStamp(genre.getUpdatedAt());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
