package com.sciensa.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.*;

import java.sql.Timestamp;
import java.util.Date;

public class DateAndTimeUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateAndTimeUtils.class);

    public static Timestamp convertOffsetInTimeStamp(OffsetDateTime dateTime){
        if(dateTime == null){
            return null;
        }

        try {
            //because when convert to timestamp, 3 hours is being removed
            Instant instant = dateTime.plusHours(3).toInstant();
            Timestamp timestamp = org.threeten.bp.DateTimeUtils.toSqlTimestamp(instant);
            return timestamp;
        }catch (Exception e){
            logger.error("Was not possible convert the OffsetDateTime to Timestamp");
            return null;
        }
    }

    public static OffsetDateTime convertTimeStampInOffset(Timestamp timestamp){
        if(timestamp == null)
            return null;
        try {
            Instant instant = org.threeten.bp.DateTimeUtils.toInstant(timestamp);
            OffsetDateTime dateTime = OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
            return dateTime;
        }catch (Exception e) {
            logger.error("Was not possible convert the Timestamp to OffsetDateTime");
            return null;
        }
    }

    public static Date convertLocalDateToJavaDate(LocalDate localDate) {
        if(localDate == null)
            return null;
        try{
            java.sql.Date date = org.threeten.bp.DateTimeUtils.toSqlDate(localDate);
            return date;
        }catch (Exception e){
            logger.error("Was not possible convert the LocalDate to Date");
            return null;
        }
    }

    public static LocalDate convertJavaDateToLocalDate(Date date) {
        try{
            if(date == null)
                return null;
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            return org.threeten.bp.DateTimeUtils.toLocalDate(sqlDate);
        }catch (Exception e) {
            logger.error("Was not possible convert the Date to LocalDate");
            return null;
        }
    }
}
