package com.sciensa.service;

import com.sciensa.models.dto.Response;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public abstract class ApiService <T> {

    protected JpaRepository repository;

    @Transactional
    public abstract ResponseEntity<Response> create(T object);
    @Transactional
    public abstract ResponseEntity<Response> update(Long id, T object);
    public abstract ResponseEntity<Response> getById(Long id);
    public abstract ResponseEntity<Response> getAll(Integer page, Integer size, String search);
    @Transactional
    public abstract ResponseEntity delete(Long id);

    public ApiService() {
    }

    public ApiService(JpaRepository repository) {
        this.repository = repository;
    }

    public JpaRepository getRepository() {
        return repository;
    }

    public void setRepository(JpaRepository repository) {
        this.repository = repository;
    }
}
