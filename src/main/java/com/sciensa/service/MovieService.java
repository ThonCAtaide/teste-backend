package com.sciensa.service;

import com.sciensa.models.dto.*;
import com.sciensa.repository.MovieRepository;
import com.sciensa.utils.DateAndTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.threeten.bp.OffsetDateTime;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService extends ApiService<Movie> {

    private static final Logger logger = LoggerFactory.getLogger(MovieService.class);

    private JpaRepository genreRepository;
    private JpaRepository artistRepository;

    @Autowired
    public MovieService(MovieRepository repository, JpaRepository genreRepository, JpaRepository artistRepository) {
        super(repository);
        this.genreRepository = genreRepository;
        this.artistRepository = artistRepository;
    }

    @Override
    public ResponseEntity<Response> create(Movie movie) {
        try{
            com.sciensa.models.entities.Movie movieEntity = new com.sciensa.models.entities.Movie();
            List<String> errorMessages = checkMovieParameters(movie, movieEntity);
            if(!errorMessages.isEmpty()){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }
            movieEntity.setCreatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
            com.sciensa.models.entities.Movie databaseResponse = (com.sciensa.models.entities.Movie) repository.save(movieEntity);

            Movie response = new Movie(databaseResponse);
            ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
            return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
        }catch (Exception e) {
            logger.error("Error, was not possible save movie - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to save the movie"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> update(Long id, Movie movie) {
        try{
            com.sciensa.models.entities.Movie movieEntity = new com.sciensa.models.entities.Movie();
            List<String> errorMessages = checkMovieParameters(movie, movieEntity);
            if(id < 0){
                errorMessages.add("Id should be a positive number.");
            }
            if(!errorMessages.isEmpty()){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }

            Object movieRecord = repository.findById(id).orElse(null);
            if(movieRecord == null){
                logger.info("Movie doesn't exist");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("The genre doesn' exist"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }else {
                movieEntity.setId(id);
                movieEntity.setCreatedAt(((com.sciensa.models.entities.Movie) movieRecord).getCreatedAt());
                movieEntity.setUpdatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
                com.sciensa.models.entities.Movie databaseResponse = (com.sciensa.models.entities.Movie) repository.save(movieEntity);

                Movie response = new Movie(databaseResponse);
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
            }
        }catch (Exception e) {
            logger.error("Error, was not possible update movie - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to update the movie"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getById(Long id) {
        try {
            if(id < 0){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("The Id should be a positive number"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }

            Object record = repository.findById(id).orElse(null);
            ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
            if(record == null){
                return new ResponseEntity<>(new ResponseObject(null, meta), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(new ResponseObject(new Movie((com.sciensa.models.entities.Movie) record), meta), HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("Error, was not possible get the movie - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the movie"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getAll(Integer page, Integer size, String search) {

        try {
            List<String> errorMessages = new ArrayList<>();
            if(page < 1) {
                errorMessages.add("page should be bigger than 0");
            }

            if(size < 1) {
                errorMessages.add("size should be bigger than 0");
            }
            if (!errorMessages.isEmpty()) {
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }

            search = search == null? "":search;
            List<com.sciensa.models.entities.Movie> movies = ((MovieRepository) repository).findAllByTitleContains(search, PageRequest.of(page - 1, size));
            List<Record> records  = movies.stream().map(Movie::new).collect(Collectors.toList());
            ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), records.size(), page, size);
            return new ResponseEntity<>(new ResponseListObject(records, meta), HttpStatus.OK);
        }catch (Exception e) {
            logger.error("Error, was not possible fetch the movies - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to fetch movies"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity delete(Long id) {
        try{
            if(id < 1){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("The Id should be a positive number"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }

            Object aux = repository.findById(id).orElse(null);
            if(aux == null){
                logger.info("The movie doesn't exist");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("The movie doesn't exist"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                com.sciensa.models.entities.Movie movie = (com.sciensa.models.entities.Movie) aux;
                repository.delete(movie);
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }

        }catch (Exception e){
            logger.error("Error, was not possible delete movie - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to delete movie"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    private List<String> checkMovieParameters(Movie movie, com.sciensa.models.entities.Movie movieEntity) {
        com.sciensa.models.entities.Artist director = getDirector(movie.getDirector());
        List<com.sciensa.models.entities.Artist> cast = getCast(movie.getCast());
        List<com.sciensa.models.entities.Genre> genres = getGenres(movie.getGenres());

        movieEntity.setDirector(director);
        movieEntity.setCast(cast);
        movieEntity.setGenres(genres);
        movieEntity.setTitle(movie.getTitle());
        movieEntity.setReleaseYear(movie.getReleaseYear());
        List<String> errorMessages = new ArrayList<>();

        if(director == null){
            errorMessages.add("Director can't be null");
        }
        if(cast.size() != movie.getCast().size()){
            errorMessages.add("Some artist of cast doesn't exist");
        }
        if(genres.size() != movie.getGenres().size()){
            errorMessages.add("Some genre passed is incorrect");
        }
        if(movie.getTitle() == null){
            errorMessages.add("Title can't be null");
        }
        if(movie.getReleaseYear() == null){
            errorMessages.add("Released year can't be null");
        }
        return errorMessages;
    }

    private com.sciensa.models.entities.Artist getDirector(Artist artistDto) {
        Object artist = artistRepository.findById(artistDto.getId()).orElse(null);

        if(artist != null)
            return (com.sciensa.models.entities.Artist) artist;
        return null;
    }

    private List<com.sciensa.models.entities.Artist> getCast(List<Artist> castDto) {
        List<com.sciensa.models.entities.Artist> artists = new ArrayList<>();
        castDto.forEach(artist -> {
            Object aux = artistRepository.findById(artist.getId()).orElse(null);
            if(aux != null){
                artists.add((com.sciensa.models.entities.Artist)  aux);
            }
        });
        return artists;
    }

    private List<com.sciensa.models.entities.Genre> getGenres(List<Genre> genresDto) {
        List<com.sciensa.models.entities.Genre> genres = new ArrayList<>();
        genresDto.forEach(artist -> {
            Object aux = genreRepository.findById(artist.getId()).orElse(null);
            if(aux != null){
                genres.add((com.sciensa.models.entities.Genre)  aux);
            }
        });
        return genres;
    }

}
