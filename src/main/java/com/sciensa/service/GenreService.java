package com.sciensa.service;

import com.sciensa.models.dto.*;
import com.sciensa.repository.GenreRepository;
import com.sciensa.utils.DateAndTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.threeten.bp.OffsetDateTime;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenreService extends ApiService<Genre> {

    private static final Logger logger = LoggerFactory.getLogger(GenreService.class);

    @Autowired
    public GenreService(GenreRepository repository) {
        super(repository);
    }

    @Override
    public ResponseEntity<Response> create(Genre genre) {
        logger.info("Genre received to save: {}", genre);
        try{
            if(genre.getDescription() == null){
                logger.info("Genre description is null");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Description can't be null"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                com.sciensa.models.entities.Genre genreEntity = new com.sciensa.models.entities.Genre(genre);
                genreEntity.setCreatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
                genreEntity.setUpdatedAt(null);
                com.sciensa.models.entities.Genre responseDatabase = (com.sciensa.models.entities.Genre) repository.save(genreEntity);

                Genre response = new Genre(responseDatabase);
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error, was not possible save genre - {} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to save the genre"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> update(Long id, Genre genre) {
        try{
            List<String> errorMessages = new ArrayList<>();
            if(genre.getDescription() == null)
                errorMessages.add("Description can't be null");

            if(id < 0){
                errorMessages.add("Invalid id");
            }

            if(!errorMessages.isEmpty()) {
                logger.info("Invalid params were found {}", genre);
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }
            Object genreRecord = repository.findById(id).orElse(null);
            if(genreRecord == null){
                logger.info("Genre doesn't exist");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("The genre doesn' exist"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }else{
                genre.setId(id);
                com.sciensa.models.entities.Genre genreEntity = new com.sciensa.models.entities.Genre(genre);
                genreEntity.setCreatedAt(((com.sciensa.models.entities.Genre) genreRecord).getCreatedAt());
                com.sciensa.models.entities.Genre responseDatabase = (com.sciensa.models.entities.Genre) repository.save(genreEntity);

                Genre response = new Genre(responseDatabase);
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
            }
        }catch (Exception e){
            logger.error("An Error occurred and was not possible update the genre - ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to update the genre"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getById(Long id) {
        try {
            if (id < 0) {
                logger.info("Invalid params were found {}", id);
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Id should be a postitive number"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                com.sciensa.models.entities.Genre responseGenrer = null;
                Object response = repository.findById(id).orElse(null);

                if(response != null)
                    responseGenrer = (com.sciensa.models.entities.Genre) response;

                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(new Genre(responseGenrer), meta), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("An error occurred trying to get genrer: {}", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the genre"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getAll(Integer page, Integer size, String search) {

        List<String> errorMessages = new ArrayList<>();
        if(page < 1) {
            errorMessages.add("page should be bigger than 0");
        }

        if(size < 1) {
            errorMessages.add("size should be bigger than 0");
        }

        try {
            if (!errorMessages.isEmpty()) {
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                PageRequest pageRequest = PageRequest.of(page-1, size);
                search = search == null? "": search;
                List<com.sciensa.models.entities.Genre> genresEntity = ((GenreRepository) repository).findAllByDescriptionContains(search, pageRequest);
                List<Record> genres = genresEntity.stream().map(Genre::new).collect(Collectors.toList());
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), genres.size(), page, size);
                return new ResponseEntity<>(new ResponseListObject(genres, meta), HttpStatus.OK);
            }
        }catch (Exception e) {
            logger.error("An error occurred: ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the genres"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
