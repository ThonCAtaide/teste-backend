package com.sciensa.service;

import com.sciensa.models.dto.*;
import com.sciensa.repository.ArtistRepository;
import com.sciensa.repository.GenreRepository;
import com.sciensa.utils.DateAndTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.OffsetDateTime;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArtistService extends ApiService<Artist> {

    private static final Logger logger = LoggerFactory.getLogger(ArtistService.class);

    @Autowired
    public ArtistService(ArtistRepository repository) {
        super(repository);
    }

    @Override
    public ResponseEntity<Response> create(Artist artist) {
        logger.info("Artist received to save: {}", artist);
        List<String> errorMessages = artistVerification(artist);
        try{
            if(!errorMessages.isEmpty()){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                com.sciensa.models.entities.Artist artistEntity = new com.sciensa.models.entities.Artist(artist);
                artistEntity.setCreatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
                artistEntity.setUpdatedAt(null);

                com.sciensa.models.entities.Artist responseDatabase = (com.sciensa.models.entities.Artist) repository.save(artistEntity);
                Artist response = new Artist(responseDatabase);
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error, was not possible save the artist - {0} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to save the artist"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> update(Long id, Artist artist) {
        logger.info("Artist received to update: {}", artist);
        List<String> errorMessages = artistVerification(artist);
        if(id == null){
            errorMessages.add("Id cound't be null");
        }

        try{
            if(!errorMessages.isEmpty()){
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            }
            Object artistRecord = repository.findById(id).orElse(null);
            if(artistRecord == null){
                logger.info("Artist doesn't exists.");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Artist doesn't exists."));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else{
                artist.setId(id);
                com.sciensa.models.entities.Artist artistEntity = new com.sciensa.models.entities.Artist(artist);
                artistEntity.setCreatedAt(((com.sciensa.models.entities.Artist) artistRecord).getCreatedAt());
                artistEntity.setUpdatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
                com.sciensa.models.entities.Artist responseDatabase = (com.sciensa.models.entities.Artist) repository.save(artistEntity);

                Artist response = new Artist(responseDatabase);
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                return new ResponseEntity<>(new ResponseObject(response, meta), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error, was not possible update the artist - {0} ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to update the artist"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getById(Long id) {
        try {
            if (id < 0) {
                logger.info("Invalid params were found {}", id);
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Id should be a postitive number"));
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                com.sciensa.models.entities.Artist responseArtist = null;
                Object response = repository.findById(id).orElse(null);

                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
                if(response == null){
                    return new ResponseEntity<>(new ResponseObject(null, meta), HttpStatus.OK);
                }
                responseArtist = (com.sciensa.models.entities.Artist) response;
                return new ResponseEntity<>(new ResponseObject(new Artist(responseArtist), meta), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("An error occurred trying to get the artist: {}", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the artist"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Response> getAll(Integer page, Integer size, String search) {
        List<String> errorMessages = new ArrayList<>();
        if(page < 1) {
            errorMessages.add("page should be bigger than 0");
        }

        if(size < 1) {
            errorMessages.add("size should be bigger than 0");
        }

        try {
            if (!errorMessages.isEmpty()) {
                logger.info("Invalid params were found");
                ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), errorMessages);
                return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                PageRequest pageRequest = PageRequest.of(page-1, size);
                search = search == null? "": search;
                List<com.sciensa.models.entities.Artist> genresEntity = ((ArtistRepository) repository).findAllByFirstNameContainsOrLastNameContains(search, search, pageRequest);
                List<Record> artists = genresEntity.stream().map(Artist::new).collect(Collectors.toList());
                ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), artists.size(), page, size);
                return new ResponseEntity<>(new ResponseListObject(artists, meta), HttpStatus.OK);
            }
        }catch (Exception e) {
            logger.error("An error occurred: ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the genres"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Response> getArtistFilmography(Long artistId){

        if (artistId < 0) {
            logger.info("Invalid params were found {}", artistId);
            ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Id should be a postitive number"));
            return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
        }

        if(repository.findById(artistId).orElse(null) == null){
            logger.info("Artist doesn't exist {}", artistId);
            ResponseError responseError = new ResponseError(400, HttpStatus.BAD_REQUEST.toString(), Collections.singletonList("Artist doesn't exist"));
            return new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
        }

        try {
            com.sciensa.models.entities.Artist artist = (com.sciensa.models.entities.Artist) repository.findById(artistId).orElse(null);
            List<Record> movies = artist.getMovies().stream().map(Movie::new).collect(Collectors.toList());
            ResponseMeta meta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), movies.size(), null, null);
            return new ResponseEntity<>(new ResponseListObject(movies, meta), HttpStatus.OK);
        }catch (Exception e) {
            logger.error("An error occurred: ", e);
            ResponseError responseError = new ResponseError(500, HttpStatus.INTERNAL_SERVER_ERROR.toString(), Collections.singletonList("Error trying to get the movies from artist"));
            return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private List<String> artistVerification(Artist artist){
        List<String> errorMessages = new ArrayList<>();

        if(artist.getFirstName() == null){
            errorMessages.add("First name can't be null");
        }

        if(artist.getLastName() == null){
            errorMessages.add("Last name can't be null");
        }

        if(artist.getDateOfBirth() == null) {
            errorMessages.add("BirthDay can't be null");
        }

        return errorMessages;
    }
}
