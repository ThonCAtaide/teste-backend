package com.sciensa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.TimeZone;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = {"com.sciensa.service", "com.sciensa.controllers", "com.sciensa.config", "com.sciensa.repository"})
public class BackendTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackendTestApplication.class, args);
    }

}
