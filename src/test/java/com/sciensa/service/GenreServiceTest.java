package com.sciensa.service;

import com.mysql.cj.util.TestUtils;
import com.sciensa.models.dto.*;
import com.sciensa.repository.GenreRepository;
import com.sciensa.utils.DateAndTimeUtils;
import com.sciensa.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.threeten.bp.OffsetDateTime;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GenreServiceTest {

    @InjectMocks
    private GenreService genreService;
    @Mock
    private GenreRepository genreRepository;

    private Genre genre;

    @Test
    public void createGenreSuccess() throws UnknownHostException {
        mockGenreCreated();
        com.sciensa.models.entities.Genre genreEntity = new com.sciensa.models.entities.Genre();
        genreEntity.setId(1L);
        genreEntity.setDescription(genre.getDescription());
        genreEntity.setCreatedAt(DateAndTimeUtils.convertOffsetInTimeStamp(OffsetDateTime.now()));
        when(genreRepository.save(any(com.sciensa.models.entities.Genre.class))).thenReturn(genreEntity);
        ResponseEntity<Response> responseResponseEntity = genreService.create(genre);

        assertEquals(HttpStatus.OK, responseResponseEntity.getStatusCode());
        ResponseObject body = (ResponseObject) responseResponseEntity.getBody();
        ResponseMeta expectedMeta = new ResponseMeta("v1", OffsetDateTime.now(), InetAddress.getLocalHost().getHostName(), 1, 1, 1);
        assertResponseEntityMeta(expectedMeta, body.getMeta());
        Genre record = (Genre) body.getRecord();

        assertEquals(record.getId(), genreEntity.getId());
        assertEquals(record.getDescription(), genreEntity.getDescription());
        assertNotNull(record.getCreatedAt());
    }

    private void assertResponseEntityMeta(ResponseMeta expected, ResponseMeta response){
        assertEquals(expected.getVersion(), response.getVersion());
        assertEquals(expected.getHostname(), response.getHostname());
        assertEquals(expected.getNumberOfRecords(), response.getNumberOfRecords());
        assertEquals(expected.getPage(), response.getPage());
        assertEquals(expected.getSize(), response.getSize());
    }

    private void assertResponseEntityError(ResponseError expect, ResponseError response){
        assertEquals(expect.getCode(), response.getCode());
        assertEquals(expect.getType(), response.getType());
        if(expect.getMessages().size() > 0){
            expect.getMessages().forEach(s -> {
                assertTrue(response.getMessages().contains(s));
            });
        }
        assertEquals(expect.getMessages().size(), response.getMessages().size());
    }


    private void mockGenreCreated(){
        genre = new Genre();
        genre.setDescription("Comedy");
    }

    private void mockGenreUpdate(){
        genre = new Genre();
        genre.setId(1L);
        genre.setDescription("Comedy");
    }


}
